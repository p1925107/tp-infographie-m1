#include <array>
#include <ostream>

#include "mathematics.h"

class Mat
{
private:
    std::array<double, 9> m;
public:
    /**
     * @brief Constructs a null Matrix
     */
    Mat();
    /**
     * @brief Construct a new Matrix from an array
     * 
     * @param data the matrix data
     */
    Mat(std::array<double, 9> data);
    /**
     * @brief Construct a new Matrix from an initializer list
     * 
     * @param data the matrix data
     */
    Mat(std::initializer_list<double> data);
    /**
     * @brief Construct a new Matrix from another Matrix
     * 
     * @param other the other Matrix
     */
    Mat(const Mat& other);
    /**
     * @brief Construct a new Matrix from another Matrix (by movement)
     * 
     * @param other the other Matrix
     */
    Mat(const Mat&& other);

    /**
     * @brief Assign a Matrix to this Matrix
     * 
     * @param other the other Matrix
     * @return const Mat& the assigned Matrix
     */
    const Mat& operator=(const Mat& other);
    /**
     * @brief Assign a Matrix to this Matrix (by movement)
     * 
     * @param other the other Matrix
     * @return const Mat& the assigned Matrix
     */
    const Mat& operator=(Mat&& other);

    /**
     * @brief Add a Matrix to this Matrix
     * 
     * @param other the other Matrix
     * @return Mat the sum of the two Matrices
     */
    Mat operator+(const Mat& other) const;
    /**
     * @brief Subtract a Matrix from this Matrix
     * 
     * @param other the other Matrix
     * @return Mat the difference of the two Matrices
     */
    Mat operator-(const Mat& other) const;
    /**
     * @brief Multiply this Matrix by a scalar
     * 
     * @param other the scalar
     * @return Mat the product of the Matrix and the scalar
     */
    Mat operator*(const double& other) const;
    /**
     * @brief Multiply this Matrix by another Matrix
     * 
     * @param other the other Matrix
     * @return Mat the product of the two Matrices
     */
    Mat operator*(const Mat& other) const;
    /**
     * @brief Multiply this Matrix by a Vector
     * 
     * @param other the Vector
     * @return Vector the product of the Matrix and the Vector
     */
    Vector operator*(const Vector& other) const;
    /**
     * @brief Divide this Matrix by a scalar
     * 
     * @param other the scalar
     * @return Mat the quotient of the Matrix and the scalar
     */
    Mat operator/(const double& other) const;


    const Mat& operator+=(const Mat& other);
    const Mat& operator-=(const Mat& other);
    const Mat& operator*=(const Mat& other);
    const Mat& operator*=(const double& other);
    const Mat& operator/=(const double& other);

    /**
     * @brief Returns a scalar from the Matrix
     * 
     * @param y the row
     * @param x the column
     * @return const double the scalar
     */
    const double operator()(int y, int x) const;

    /**
     * @brief Sets a scalar in the Matrix
     * 
     * @param x the column
     * @param y the row
     * @param value the value to set
     */
    void set(int x, int y, double value);

    /**
     * @brief Returns the inverse matrix
     * 
     * @return Mat the inverse matrix
     */
    Mat invert() const;

    /**
     * @brief Returns the transposed matrix
     * 
     * @return Mat the transposed matrix
     */
    Mat transpose() const;

    /**
     * @brief Returns the determinant of the matrix
     * 
     * @return double the determinant
     */
    double det() const;

    ~Mat();

    friend std::ostream &operator<<(std::ostream &os, const Mat &rhs);
};


Mat operator*(double scalar, Mat matrix);

/**
 * @brief Generates a scaling matrix
 * 
 * @param x scale factor in x
 * @param y scale factor in y
 * @param z scale factor in z
 * @return Mat the scaling matrix 
 */
Mat Scale(double x, double y, double z);

/**
 * @brief Generates a rotation matrix in x
 * 
 * @param angle the angle of rotation
 * @return Mat the rotation matrix
 */
Mat RotateX(double angle);

/**
 * @brief Generates a rotation matrix in y
 * 
 * @param angle the angle of rotation
 * @return Mat the rotation matrix
 */
Mat RotateY(double angle);

/**
 * @brief Generates a rotation matrix in z
 * 
 * @param angle the angle of rotation
 * @return Mat the rotation matrix
 */
Mat RotateZ(double angle);

/**
 * @brief Generates a rotation matrix around an arbitrary axis
 * 
 * @param angle the angle of rotation
 * @param x the x component of the axis
 * @param y the y component of the axis
 * @param z the z component of the axis
 * @return Mat the rotation matrix
 */
Mat Rotate(double angle, double x, double y, double z);

/**
 * @brief Generates a perspective projection matrix
 * 
 * @param fovy the field of view in the y direction
 * @param aspect the aspect ratio
 * @param zNear the near clipping plane
 * @param zFar the far clipping plane
 * @return Mat the perspective projection matrix
 */
Mat Perspective(double fovy, double aspect, double zNear, double zFar);

/**
 * @brief Generates an orthographic projection matrix
 * 
 * @param left the left clipping plane
 * @param right the right clipping plane
 * @param bottom the bottom clipping plane
 * @param top the top clipping plane
 * @param zNear the near clipping plane
 * @param zFar the far clipping plane
 * @return Mat the orthographic projection matrix
 */
Mat Ortho(double left, double right, double bottom, double top, double zNear, double zFar);
