#pragma once

#include "mesh.h"
#include "mathematics.h"
#include "extern/FastNoiseLite.h"
#include "color.h"

class SphereTerrain
{
private:
    Mesh mesh;
    int face_res;
    int face_size;

    /**
     * @brief Return the index of the face corresponding to the given direction
     * 
     * @param v the direction
     * @return int the index of the face
     */
    int getVertexFace(Vector v) const;

    /**
     * @brief Get the index of the neighbour vertex in an adjacent face
     * 
     * Returns -1 if the vertex is a corner
     * 
     * @param face the index of the face the vertex is in
     * @param face_i the index of the vertex in the face
     * @return int the global index of the neighbour vertex
     */
    int getNeighbourAtBoundary(int face, int face_i) const;

    /**
     * @brief Get the indices of the 4 neighbours of a vertex
     * 
     * The neighbours are ordered clockwise to allow computing the normal
     * If a neighbour is not present, the index is -1
     * 
     * @param i the index of the vertex
     * @return std::vector<int> the indices of the neighbours oredered clockwise
     */
    std::vector<int> getVertexNeighbours(int i) const;

    /**
     * @brief Get the nearest vertex to a given direction
     * 
     * @param v the direction
     * @return int the index of the nearest vertex
     */
    int getNearestVertex(Vector v) const;

    /**
     * @brief Update the normals of the mesh
     * 
     */
    void updateNormals();

    /**
     * @brief Update the normal at the given index
     * 
     * @param i the index of the vertex
     */
    void updateNormal(int i);

public:
    SphereTerrain(int face_res = 20);
    ~SphereTerrain();

    /**
     * @brief Get the underlying mesh
     * 
     * @return const Mesh& the mesh
     */
    const Mesh& getMesh() const;

    /**
     * @brief Apply a noise to the SphereTerrain
     * 
     * 
     * This function takes in a FastNoiseLite object as a 3D noise generator
     * and applies it to the SphereTerrain using the vertices coordinates as
     * the input for the noise generator.
     * 
     * @param noise The noise to apply
     * @return a reference to itself
     */
    SphereTerrain& applyNoise(FastNoiseLite& noise, double scale);

    /**
     * @brief Flatten the terrain around a direction
     * 
     * 
     * This function will flatten the terrain around a direction
     * and will target the nearest vertex's height
     * 
     * @param center The center of the flattening
     * @param radius The angular radius of the flattening
     * @param force The force of the flattening (0 to 1)
     * 
     * @return a reference to itself
     */
    SphereTerrain& applyFlattening(
        Vector center,
        double radius,
        double force,
        double target_height = -1
    );

    std::vector<Color> getDebugMeshColor() const;

    std::vector<Color> getMeshColor() const;
};

