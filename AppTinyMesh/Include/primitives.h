#pragma once
#include "mesh.h"

namespace primitives {
    Mesh initCubeSphere(int res_face = 20);
    Mesh initSphere(int res_xz = 25, int res_y = 25);
    Mesh initHalfSphere(int res_xz = 25, int res_y = 13);
    Mesh initTore(double radius = 0.75, double thickness = 0.25, int res_hring = 25, int res_vring = 25);
    Mesh initCyllinder(double height = 1, int res_disque = 25);
    Mesh initCapsule(double height = 1, int res_disque = 25, int res_dome = 13);

}