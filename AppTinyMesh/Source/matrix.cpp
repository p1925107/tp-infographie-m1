#include "matrix.h"

#include <cmath>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <assert.h>

Mat::Mat()
{
    m.fill(0);
}

Mat::Mat(std::array<double, 9> data) 
: m(data)
{}

Mat::Mat(std::initializer_list<double> data) {
    if (data.size() != 9) {
        return;
    }

    for (int i = 0; i < 9; i++) {
        m[i] = *(data.begin() + i);
    }
}

Mat::Mat(const Mat& other)
: m(other.m)
{}

Mat::Mat(const Mat&& other)
{
    m = std::move(other.m);
}

const Mat& Mat::operator=(const Mat& other) {
    m = other.m;
    return *this;
}

const Mat& Mat::operator=(Mat&& other) {
    m = std::move(other.m);
    return *this;
}

Mat Mat::operator+(const Mat& other) const {
    Mat ret;
    for (int i = 0; i < 9; i++) {
        ret.m[i] = m[i] + other.m[i];
    }
    return ret;
}

Mat Mat::operator-(const Mat& other) const {
    Mat ret;
    for (int i = 0; i < 9; i++) {
        ret.m[i] = m[i] - other.m[i];
    }
    return ret;
}

Mat Mat::operator*(const double& other) const {
    Mat ret;
    for (int i = 0; i < 9; i++) {
        ret.m[i] = m[i] * other;
    }
    return ret;
}

Mat Mat::operator*(const Mat& other) const {
    Mat ret;
    for (int y = 0; y < 3; y++) 
    for (int x = 0; x < 3; x++) {
        double sum = 0;
        for (int i = 0; i < 3; i++) {
            sum += (*this)(y,i) * other(i,x);
        }
        ret.set(x,y,sum);
    }
    return ret;
}

Vector Mat::operator*(const Vector& other) const {
    Vector ret;
    for (int y = 0; y < 3; y++) {
        double sum = 0;
        for (int i = 0; i < 3; i++) {
            sum += (*this)(y,i) * other[i];
        }
        ret[y] = sum;
    }
    return ret;
}

Mat Mat::operator/(const double& other) const {
    Mat ret;
    for (int i = 0; i < 9; i++) {
        ret.m[i] = m[i] / other;
    }
    return ret;
}

const Mat& Mat::operator+=(const Mat& other) {
    return (*this) = (*this) + other;
}

const Mat& Mat::operator-=(const Mat& other) {
    return (*this) = (*this) - other;
}

const Mat& Mat::operator*=(const Mat& other) {
    return (*this) = (*this) * other;
}

const Mat& Mat::operator*=(const double& other) {
    return (*this) = (*this) * other;
}

const Mat& Mat::operator/=(const double& other) {
    return (*this) = (*this) / other;
}

const double Mat::operator()(int y, int x) const {
    return m[y * 3 + x];
}

void Mat::set(int x, int y, double value) {
    m[y*3 + x] = value;
}

Mat Mat::invert() const {
    double det = this->det();
    if (std::abs(det) < 1e-4) {
        std::cout << "Singular matrix in invert!" << std::endl;
        exit(1);
    }

    Mat comatrix;
    for (int y = 0; y < 3; y++)
    for (int x = 0; x < 3; x++) {
        comatrix.set(y, x,
            + (*this)((y+1) % 3,(x+1) % 3) * (*this)((y+2) % 3, (x+2) % 3)
            - (*this)((y+1) % 3,(x+2) % 3) * (*this)((y+2) % 3, (x+1) % 3)
        );
    }

    return (1.0/det) * comatrix;
}

// Transpose
Mat Mat::transpose() const {
    Mat ret;
    for (int y = 0; y < 3; y++)
    for (int x = 0; x < 3; x++) {
        ret.set(y,x,(*this)(x,y));
    }
    return ret;
}

std::ostream &operator<<(std::ostream &os, const Mat &rhs) {
    os << "------------------------" << std::endl;
    for (int y = 0; y < 3; y++) {
        for (int x = 0; x < 3; x++) {
            os << "  " << std::setw(6) << std::setprecision(4) << rhs(y,x);
        }
        os << std::endl;
    }
    os << "------------------------" << std::endl;
    return os;
}

double Mat::det() const {
    double sum = 0;

    for (int i = 0; i < 3; i++) {
        double prod = 1;
        for (int j = 0; j < 3; j++) {
            int y = j;
            int x = (i+j) % 3;
            prod *= (*this)(y,x);
        }
        sum += prod;
    }

    for (int i = 0; i < 3; i++) {
        double prod = 1;
        for (int j = 0; j < 3; j++) {
            int y = 2-j;
            int x = (i+j) % 3;
            prod *= (*this)(y,x);
        }
        sum -= prod;
    }

    return sum;
}

Mat::~Mat()
{}

Mat operator*(double scalar, Mat matrix) {
    return matrix * scalar;
}

Mat Scale(double x, double y, double z) {
    return Mat({
        x, 0, 0,
        0, y, 0,
        0, 0, z
    });
}

Mat RotateX(double angle) {
    return Mat({
        1, 0,          0,
        0, cos(angle), -sin(angle),
        0, sin(angle), cos(angle)
    });
}

Mat RotateY(double angle) {
    return Mat({
        cos(angle), 0, sin(angle),
        0,          1, 0,
        -sin(angle),0, cos(angle)
    });
}

Mat RotateZ(double angle) {
    return Mat({
        cos(angle), -sin(angle), 0,
        sin(angle), cos(angle),  0,
        0,          0,           1
    });
}

Mat Rotate(double angle, double x, double y, double z) {
    double c = cos(angle);
    double s = sin(angle);
    double t = 1 - c;
    return Mat({
        t*x*x + c,   t*x*y - s*z, t*x*z + s*y,
        t*x*y + s*z, t*y*y + c,   t*y*z - s*x,
        t*x*z - s*y, t*y*z + s*x, t*z*z + c
    });
}

Mat Perspective(double fovy, double aspect, double zNear, double zFar) {
    double f = 1.0 / tan(fovy / 2.0);
    return Mat({
        f/aspect, 0, 0, 0,
        0, f, 0, 0,
        0, 0, (zFar+zNear)/(zNear-zFar), 2*zFar*zNear/(zNear-zFar),
        0, 0, -1, 0
    });
}

Mat Ortho(double left, double right, double bottom, double top, double zNear, double zFar) {
    return Mat({
        2.0/(right-left), 0, 0, -(right+left)/(right-left),
        0, 2.0/(top-bottom), 0, -(top+bottom)/(top-bottom),
        0, 0, -2.0/(zFar-zNear), -(zFar+zNear)/(zFar-zNear),
        0, 0, 0, 1
    });
}