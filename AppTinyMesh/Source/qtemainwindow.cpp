#include "qte.h"
#include "implicits.h"
#include "primitives.h"
#include "ui_interface.h"
#include "sphere_terrain.h"
#include "extern/FastNoiseLite.h"

MainWindow::MainWindow() : QMainWindow(), uiw(new Ui::Assets)
{
	// Chargement de l'interface
    uiw->setupUi(this);

	// Chargement du GLWidget
	meshWidget = new MeshWidget;
	QGridLayout* GLlayout = new QGridLayout;
	GLlayout->addWidget(meshWidget, 0, 0);
	GLlayout->setContentsMargins(0, 0, 0, 0);
    uiw->widget_GL->setLayout(GLlayout);

	// Creation des connect
	CreateActions();

	meshWidget->SetCamera(Camera(Vector(10, 0, 0), Vector(0.0, 0.0, 0.0)));
}

MainWindow::~MainWindow()
{
	delete meshWidget;
}

void MainWindow::CreateActions()
{
	// Buttons
    // Primitives
    connect(uiw->boxMesh, SIGNAL(clicked()), this, SLOT(BoxMeshExample()));
    connect(uiw->uvsphereMesh, SIGNAL(clicked()), this, SLOT(UvSphereMeshExample()));
    connect(uiw->cubesphereMesh, SIGNAL(clicked()), this, SLOT(CubeSphereMeshExample()));
    connect(uiw->toreMesh, SIGNAL(clicked()), this, SLOT(ToreMeshExample()));
    connect(uiw->cyllinderMesh, SIGNAL(clicked()), this, SLOT(CyllindreMeshExample()));
    connect(uiw->capsuleMesh, SIGNAL(clicked()), this, SLOT(CapsuleMeshExample()));

    // Complex objects
    connect(uiw->bobMesh, SIGNAL(clicked()), this, SLOT(BobMeshExample()));
    connect(uiw->orbMesh, SIGNAL(clicked()), this, SLOT(OrbMeshExample()));
    connect(uiw->closedCyllinderMesh, SIGNAL(clicked()), this, SLOT(ClosedCyllindreMeshExample()));
    connect(uiw->sphereTerrainMesh, SIGNAL(clicked()), this, SLOT(SphereTerrainExample()));

    // Implicit
    connect(uiw->sphereImplicit, SIGNAL(clicked()), this, SLOT(SphereImplicitExample()));

    connect(uiw->resetcameraButton, SIGNAL(clicked()), this, SLOT(ResetCamera()));
    connect(uiw->wireframe, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
    connect(uiw->radioShadingButton_1, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
    connect(uiw->radioShadingButton_2, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));

	// Widget edition
	connect(meshWidget, SIGNAL(_signalEditSceneLeft(const Ray&)), this, SLOT(editingSceneLeft(const Ray&)));
	connect(meshWidget, SIGNAL(_signalEditSceneRight(const Ray&)), this, SLOT(editingSceneRight(const Ray&)));
}

void MainWindow::editingSceneLeft(const Ray&)
{
}

void MainWindow::editingSceneRight(const Ray&)
{
}

void MainWindow::BoxMeshExample()
{
	Mesh boxMesh = Mesh(Box(1.0));

	std::vector<Color> cols;
	cols.resize(boxMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(boxMesh, cols, boxMesh.VertexIndexes());
	UpdateGeometry();
}

void MainWindow::UvSphereMeshExample()
{
  Mesh sphereMesh = primitives::initSphere();

  std::vector<Color> cols;
  cols.resize(sphereMesh.Vertexes());
  for (size_t i = 0; i < cols.size(); i++)
	cols[i] = Color(0.2,0.2,0.8);

  meshColor = MeshColor(sphereMesh, cols, sphereMesh.VertexIndexes());
  UpdateGeometry();
}

void MainWindow::CubeSphereMeshExample()
{
  Mesh sphereMesh = primitives::initCubeSphere();

  std::vector<Color> cols;
  cols.resize(sphereMesh.Vertexes());
  for (size_t i = 0; i < cols.size(); i++)
  cols[i] = Color(0.2,0.2,0.8);

  meshColor = MeshColor(sphereMesh, cols, sphereMesh.VertexIndexes());
  UpdateGeometry();
}

void MainWindow::ToreMeshExample()
{
  Mesh toreMesh = primitives::initTore(1, 0.1, 100, 25);
  std::vector<Color> cols;
  cols.resize(toreMesh.Vertexes());
  for (size_t i = 0; i < cols.size(); i++) {
    cols[i] = Color(0.2,0.2,0.8);
  }

  meshColor = MeshColor(toreMesh, cols, toreMesh.VertexIndexes());
  UpdateGeometry();
}

void MainWindow::CyllindreMeshExample()
{
  Mesh cyllindreMesh = primitives::initCyllinder(5,50);
  std::vector<Color> cols;
  cols.resize(cyllindreMesh.Vertexes());
  for (size_t i = 0; i < cols.size(); i++) {
    cols[i] = Color(0.2,0.2,0.8);
  }

  meshColor = MeshColor(cyllindreMesh, cols, cyllindreMesh.VertexIndexes());
  UpdateGeometry();
}

void MainWindow::CapsuleMeshExample()
{
  Mesh capsuleMesh = primitives::initCapsule(2);
  std::vector<Color> cols;
  cols.resize(capsuleMesh.Vertexes());
  for (size_t i = 0; i < cols.size(); i++) {
    cols[i] = Color(0.2,0.2,0.8);
  }

  meshColor = MeshColor(capsuleMesh, cols, capsuleMesh.VertexIndexes());
  UpdateGeometry();
}

void MainWindow::BobMeshExample() {
  Mesh bobMesh = Mesh(Box(1.0))
               + Mesh(Box(1.0)).Translate({2.5,0,0})
               + Mesh(Box(1.0)).Translate({2.5,2.5,0})
               + Mesh(Box(1.0)).Translate({0,2.5,0})
               + Mesh(Box(1.0)).Translate({0,0,2.5})
               + Mesh(Box(1.0)).Translate({2.5,0,2.5})
               + Mesh(Box(1.0)).Translate({2.5,2.5,2.5})
               + Mesh(Box(1.0)).Translate({0,2.5,2.5})
  ;

  std::vector<Color> cols;
	cols.resize(bobMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(bobMesh, cols, bobMesh.VertexIndexes());
	UpdateGeometry();
}

void MainWindow::OrbMeshExample() {
  Mesh sphereMesh = primitives::initSphere();
  double step = M_PI / 5.0;
  for (int i = 0; i < 5; i++) {
    sphereMesh += primitives::initTore(1.5, 0.1, 100, 20) * RotateX(step * i);
    sphereMesh += primitives::initTore(1.5, 0.1, 100, 20) * RotateZ(step * i);
    sphereMesh += primitives::initTore(1.5, 0.1, 100, 20) * RotateZ(step * i) * RotateX(M_PI_2);
  }

  std::vector<Color> cols;
  cols.resize(sphereMesh.Vertexes());
  for (size_t i = 0; i < cols.size(); i++)
	cols[i] = Color(0.2,0.2,0.8);

  meshColor = MeshColor(sphereMesh, cols, sphereMesh.VertexIndexes());
  UpdateGeometry();
}

void MainWindow::ClosedCyllindreMeshExample() {
  Mesh cyllindreMesh = primitives::initCyllinder(5,50);
  std::vector<Color> cols;
  cols.resize(cyllindreMesh.Vertexes());
  for (size_t i = 0; i < cols.size(); i++) {
    cols[i] = Color(0.2,0.2,0.8);
  }

  meshColor = MeshColor(cyllindreMesh, cols, cyllindreMesh.VertexIndexes());
  UpdateGeometry();
}

void MainWindow::SphereTerrainExample() {
  SphereTerrain sphereTerrain = SphereTerrain(80);
  
  FastNoiseLite noise;
  noise.SetNoiseType(FastNoiseLite::NoiseType_OpenSimplex2S);
  noise.SetFrequency(1.5f);
  noise.SetSeed(330151);
  noise.SetFractalType(FastNoiseLite::FractalType_FBm);
  noise.SetFractalOctaves(4);
  noise.SetFractalLacunarity(3.0);
  noise.SetFractalGain(0.5);
  noise.SetDomainWarpType(FastNoiseLite::DomainWarpType_OpenSimplex2);
  noise.SetDomainWarpAmp(25);

  sphereTerrain.applyNoise(noise, 0.15)
               .applyFlattening(Vector::Z, M_PI_4, 0.7, 1.2)
               .applyFlattening(Vector::X, M_PI / 8.0, 1.0, 0.8)
               .applyFlattening({-0.5, 0.5, 0.5}, M_PI_4, 1.0, 1.05)
  ;

  const Mesh& sphereMesh = sphereTerrain.getMesh();

  std::vector<Color> cols = sphereTerrain.getMeshColor();

  meshColor = MeshColor(
    sphereMesh,
    cols,
    sphereMesh.VertexIndexes()
  );
  UpdateGeometry();
}

void MainWindow::SphereImplicitExample()
{
  AnalyticScalarField implicit;

  Mesh implicitMesh;
  implicit.Polygonize(31, implicitMesh, Box(2.0));

  std::vector<Color> cols;
  cols.resize(implicitMesh.Vertexes());
  for (size_t i = 0; i < cols.size(); i++)
    cols[i] = Color(0.8, 0.8, 0.8);

  meshColor = MeshColor(implicitMesh, cols, implicitMesh.VertexIndexes());
  UpdateGeometry();
}

void MainWindow::UpdateGeometry()
{
	meshWidget->ClearAll();
	meshWidget->AddMesh("BoxMesh", meshColor);

    uiw->lineEdit->setText(QString::number(meshColor.Vertexes()));
    uiw->lineEdit_2->setText(QString::number(meshColor.Triangles()));

	UpdateMaterial();
}

void MainWindow::UpdateMaterial()
{
    meshWidget->UseWireframeGlobal(uiw->wireframe->isChecked());

    if (uiw->radioShadingButton_1->isChecked())
		meshWidget->SetMaterialGlobal(MeshMaterial::Normal);
	else
		meshWidget->SetMaterialGlobal(MeshMaterial::Color);
}

void MainWindow::ResetCamera()
{
	meshWidget->SetCamera(Camera(Vector(-10.0), Vector(0.0)));
}
