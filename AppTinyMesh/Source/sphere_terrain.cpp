#include "sphere_terrain.h"
#include "primitives.h"
#include <cmath>

int SphereTerrain::getVertexFace(Vector v) const {
    double X = v.dot(Vector::X);
    double Y = v.dot(Vector::Y);
    double Z = v.dot(Vector::Z);
    // If X is the biggest component
    if (abs(X) > abs(Y) && abs(X) > abs(Z)) {
        if (X > 0) {
            return 0;
        }
        else {
            return 1;
        }
    }
    // If Y is the biggest component
    else if (abs(Y) > abs(X) && abs(Y) > abs(Z)) {
        if (Y > 0) {
            return 2;
        }
        else {
            return 3;
        }
    }
    // If Z is the biggest component
    else {
        if (Z > 0) {
            return 4;
        }
        else {
            return 5;
        }
    }

}

int SphereTerrain::getNeighbourAtBoundary(int face, int face_i) const {
    // Adjacency matrix for the faces
    static const int face_adj_mat[6][4] = {
        //up down left right
        { 3, 2, 5, 4 },
        { 3, 2, 4, 5 },
        { 5, 4, 1, 0 },
        { 4, 5, 1, 0 },
        { 3, 2, 0, 1 },
        { 3, 2, 1, 0 }
    };

    // If the vertex is a corner
    if (
        face_i == 0 // Bottom left
     || face_i == face_res // Top left
     || face_i == face_size - 1 - face_res // Bottom right
     || face_i == face_size - 1 // Top right
     ) {
        return -1;
    }
    
    // Determine which edge the vertex is on
    int edge = -1;
    if (face_i / face_res == 0) {               // Left column
        edge = 2;       
    } else if (face_i % face_res == (face_res-1)) {  // Top row
        edge = 0;    
    } else if (face_i % face_res == 0) {             // Bottom row
        edge = 1;      
    } else if (face_i / face_res == face_res-1) {    // Right column
        edge = 3;    
    } else {
        // The vertex is not on the boundary
        std::cerr 
            << "getNeighbourAtBoundary: Error: vertex is not on the boundary" 
            << std::endl
        ;
        return -1;
    }
    
    int neighbour_face = face_adj_mat[face][edge];
    if (edge == 0) {
        // If the vertex is on top,
        // the neighbour is on the bottom of the same column
        return neighbour_face * face_size + face_res * (face_i / face_res) + 1;
    } else if (edge == 1) {
        // If the vertex is on the bottom,
        // the neighbour is on top of the same column
        return neighbour_face * face_size + face_i + face_res - 2;
    } else if (edge == 2) {
        // If the vertex is on the left,
        // the neighbour is on the right of the same row
        return neighbour_face * face_size + face_size - 2*face_res + face_i;
    } else if (edge == 3) {
        // If the vertex is on the right,
        // the neighbour is on the left of the same row
        return neighbour_face * face_size + face_res + face_i % face_res;
    } else {
        // The vertex is not on the boundary
        std::cerr 
            << "getNeighbourAtBoundary: Error: vertex is not on the boundary" 
            << std::endl
        ;
        return -1;
    }
}

std::vector<int> SphereTerrain::getVertexNeighbours(int i) const {
    int face = i / face_size;
    int face_i = i % face_size;
    int row_i = face_i / face_res;
    std::vector<int> neighbours(4, -1);
    // If the vertex is not on the left or right boundary
    if (row_i > 0 && row_i < face_res-1) {
        neighbours[1] = i + face_res;
        neighbours[3] = i - face_res;
    } else {
        if (row_i == 0) {
            neighbours[1] = i + face_res;
            neighbours[3] = getNeighbourAtBoundary(face, face_i);
        } else {
            neighbours[1] = getNeighbourAtBoundary(face, face_i);
            neighbours[3] = i - face_res;
        }
    }
    // If the vertex is not on top or bottom boundary
    if (face_i % face_res > 0 && face_i % face_res < face_res-1) {
        neighbours[0] = i - 1;
        neighbours[2] = i + 1;
    } else {
        if (face_i % face_res == 0) {
            neighbours[0] = getNeighbourAtBoundary(face, face_i);
            neighbours[2] = i + 1;
        } else {
            neighbours[0] = i - 1;
            neighbours[2] = getNeighbourAtBoundary(face, face_i);
        }
    }

    return neighbours;
}

// Complexity: O(n)
int SphereTerrain::getNearestVertex(Vector v) const {
    // Get the face the vetor is pointing to
    int face = getVertexFace(v);
    int start = face * face_size;
    int end = start + face_size;
    double nearest = 9999;
    int nearest_index = -1;
    // Loop through the vertices of the face to find the nearest one
    for (int i = start; i < end; i++) {
        double dist = Norm(v - mesh.Vertex(i));
        if (dist < nearest) {
            nearest = dist;
            nearest_index = i;
        }
    }
    
    return nearest_index;

}

void SphereTerrain::updateNormals() {
    //TODO : optimize
    for (int i = 0; i < mesh.Vertexes(); i++) {
        updateNormal(i);
    }
}

void SphereTerrain::updateNormal(int i) {
    std::vector<int> neighbours = getVertexNeighbours(i);
    
    Vector u,v;
    if (neighbours[0] != -1 && neighbours[2] != -1) {
        u = Normalized(
            mesh.Vertex(neighbours[2]) - mesh.Vertex(neighbours[0])
        );
    } else if (neighbours[0] != -1) {
        u = Normalized(
            mesh.Vertex(i) - mesh.Vertex(neighbours[0])
        );
    } else if (neighbours[2] != -1) {
        u = Normalized(
            mesh.Vertex(neighbours[2]) - mesh.Vertex(i)
        );
    } else {
        std::cerr << "Cannot compute normal: vertex has no neighbours" << std::endl;
        exit(1);
    }

    if (neighbours[1] != -1 && neighbours[3] != -1) {
        v = Normalized(
            mesh.Vertex(neighbours[1]) - mesh.Vertex(neighbours[3])
        );
    } else if (neighbours[1] != -1) {
        v = Normalized(
            mesh.Vertex(neighbours[1]) - mesh.Vertex(i)
        );
    } else if (neighbours[3] != -1) {
        v = Normalized(
            mesh.Vertex(i) - mesh.Vertex(neighbours[3])
        );
    } else {
        std::cerr << "Cannot compute normal: vertex has no neighbours" << std::endl;
        exit(1);
    }

    mesh.getNormals()[i] = Normalized(u / v);
}

SphereTerrain::SphereTerrain(int face_res) :
face_res(face_res), face_size(face_res * face_res),
mesh(primitives::initCubeSphere(face_res))
{}



SphereTerrain::~SphereTerrain()
{
}

const Mesh& SphereTerrain::getMesh() const {
    return mesh;
}

SphereTerrain& SphereTerrain::applyNoise(FastNoiseLite& noise, double scale = 0.1) {
    std::vector<Vector>& vertices = mesh.getVertices();
    std::vector<Vector>& normals = mesh.getNormals();

    for (int i = 0; i < vertices.size(); i++) {
        Vector& vertex = vertices[i];

        double noise_value = noise.GetNoise(vertex[0], vertex[1], vertex[2]);

        vertex = Normalized(vertex) * (1 + noise_value * scale);
    }

    updateNormals();
    return *this;
}

SphereTerrain& SphereTerrain::applyFlattening(
        Vector center,
        double radius,
        double force,
        double target_height
    ) {
    std::vector<Vector>& vertices = mesh.getVertices();
    int target_vertex = getNearestVertex(Normalized(center));
    if (target_height < 0) {
        target_height = Norm(vertices[target_vertex]);
    }
    struct target {
        int vertex;
        double distance;
    };
    std::vector<target> targets;

    targets.push_back({target_vertex,0});
    
    for (int i = 0; i < vertices.size(); i++) {
        if (i == target_vertex) continue;
        Vector& vertex = vertices[i];
        double angle = acos(
            Normalized(vertices[target_vertex]).dot(Normalized(vertex))
        );
        if (angle < radius)
            targets.push_back({i,angle});
    }

    for (target t : targets) {
        Vector vertex = vertices[t.vertex];
        double height = Norm(vertex);
        // Fall-off function
        //https://www.desmos.com/calculator/vx2qw57jee
        double new_height = 
            height                              // Current height
            + (target_height - height)          // Height difference
              * force                           // Force
              / (t.distance+1.0)                  // Fall off
              * ((radius - t.distance) / radius)  // ...
        ;
        vertices[t.vertex] = Normalized(vertex) * new_height;
    }

    for (target t : targets) {
        updateNormal(t.vertex);
    }

    return *this;
}

std::vector<Color> SphereTerrain::getDebugMeshColor() const {
    std::vector<Color> cols;
    cols.reserve(mesh.Vertexes());
    for (int f = 0; f < 6; f++) {
        for (int i = 0; i < face_size; i++) {
            if (i / face_res == 0) {                    // Left column -> red
                cols.push_back(Color(255, 0, 0));       
            } else if (i % face_res == (face_res-1)) {  // Top row -> green
                cols.push_back(Color(0, 255, 0));       
            } else if (i % face_res == 0) {             // Bottom row -> blue
                cols.push_back(Color(0, 0, 255));       
            } else if (i / face_res == face_res-1) {    // Right column -> yellow
                cols.push_back(Color(255, 255, 0));     
            } else {
                if (f == 4) {                           // (+Z) face
                    cols.push_back(Color(255, 0, 255)); // magenta
                } else if (f == 0) {                    // (+X) face
                    cols.push_back(Color(0, 255, 255)); // cyan
                } else if (f == 2) {                    // (+Y) face
                    cols.push_back(Color(0, 0, 0));     // black
                } else {
                    cols.push_back(Color(255,255,255));
                }
            }
        }
    }

    return cols;
}

std::vector<Color> SphereTerrain::getMeshColor() const {
    std::vector<Color> cols;
    cols.reserve(mesh.Vertexes());

    Vector colors[6] {
        // Blue for water
        {0, 0, 1},
        // Yellow for sand
        {1, 1, 0},
        // Green for grass
        {0, 1, 0},
        // Brown for dirt
        {0.5, 0.25, 0},
        // Grey for rock
        {0.5, 0.5, 0.5},
        // White for snow
        {1, 1, 1}
    };
    double thresholds[6] = {
        1.0,
        1.01,
        1.05,
        1.1,
        1.12,
        1.15
    };

    for (int i = 0; i < mesh.Vertexes(); i++) {
        float height = Norm(mesh.Vertex(i));
        if (height < 1.0) {
            cols.push_back(Color(colors[0][0], colors[0][1], colors[0][2]));
        } else if (height > 1.15) {
            cols.push_back(Color(colors[5][0], colors[5][1], colors[5][2]));
        } else {
            for (int j = 0; j < 5; j++) {
                if (height < thresholds[j+1]) {
                    float t = (height - thresholds[j]) / (thresholds[j+1] - thresholds[j]);
                    cols.push_back(Color(
                        colors[j][0] * (1-t) + colors[j+1][0] * t,
                        colors[j][1] * (1-t) + colors[j+1][1] * t,
                        colors[j][2] * (1-t) + colors[j+1][2] * t
                    ));
                    break;
                }
            }
        }
    }

    return cols;
}
