#include "primitives.h"

namespace primitives {

    Mesh initCubeSphere(int res_face) {
        std::vector<Vector> cube_vertices;
        std::vector<Vector> vertices;
        std::vector<Vector> normals;
        std::vector<int> va;
        std::vector<int> na;
        // The offset of the vertices in the cube_vertices vector
        Vector centering_offset = Vector(0.5, 0.5, 0.5);

        // The vertices of the cube
        cube_vertices = {
            { 0, 0, 0 }, //0
            { 1, 0, 0 }, //1
            { 1, 1, 0 }, //2
            { 0, 1, 0 }, //3
            { 0, 0, 1 }, //4
            { 1, 0, 1 }, //5
            { 1, 1, 1 }, //6
            { 0, 1, 1 }, //7
        };

        // The faces of the cube
        int faces[6][4] = {
            { 1, 5, 6, 2 }, // 0 (+X)
            { 4, 0, 3, 7 }, // 1 (-X)
            { 3, 2, 6, 7 }, // 2 (+Y)
            { 4, 5, 1, 0 }, // 3 (-Y)
            { 5, 4, 7, 6 }, // 4 (+Z)
            { 0, 1, 2, 3 }, // 5 (-Z)
        };

        // Loop over the faces to generate the vertices, normals and indices
        for (int f = 0; f < 6; f++) {
            // Keep track of the starting index of the vertices and normals
            // for this face
            int face_start = vertices.size();

            // Get the current face's vertices
            Vector v0 = cube_vertices[faces[f][0]] - centering_offset;
            Vector v1 = cube_vertices[faces[f][1]] - centering_offset;
            Vector v2 = cube_vertices[faces[f][2]] - centering_offset;
            Vector v3 = cube_vertices[faces[f][3]] - centering_offset;

            // Generate the new face's vertices
            for (int i = 0; i < res_face; i++) {
                for (int j = 0; j < res_face; j++) {
                    Vector x = Lerp(
                        v0, v1, 
                        (double) 2*i / (double) (res_face - 1)
                    );
                    Vector y = Lerp(
                        v0, v3,
                        (double) 2*j / (double) (res_face - 1)
                    );
                    Vector v = Normalized((x + y) / 2.0);
                    vertices.push_back(v);
                    normals.push_back(v);
                }
            }

            // Generate the face's triangles
            for (int i = 0; i < res_face - 1; i++) {
                for (int j = 0; j < res_face - 1; j++) {
                    va.push_back(face_start + i * res_face + j);            // 0,0
                    va.push_back(face_start + i * res_face + j + 1);        // 0,1
                    va.push_back(face_start + (i + 1) * res_face + j + 1);  // 1,1

                    va.push_back(face_start + i * res_face + j);            // 0,0
                    va.push_back(face_start + (i + 1) * res_face + j + 1);  // 1,1
                    va.push_back(face_start + (i + 1) * res_face + j);      // 1,0

                    na.push_back(face_start + i * res_face + j);
                    na.push_back(face_start + i * res_face + j + 1);
                    na.push_back(face_start + (i + 1) * res_face + j + 1);

                    na.push_back(face_start + i * res_face + j);
                    na.push_back(face_start + (i + 1) * res_face + j + 1);
                    na.push_back(face_start + (i + 1) * res_face + j);
                }
            }
        }
        return Mesh(vertices, normals, va, na);
    }

    Mesh initSphere(int res_xz, int res_y) {
        std::vector<Vector> vertices;
        std::vector<Vector> normals;
        std::vector<int> va;
        std::vector<int> na;

        double step_xz = 2 * M_PI / res_xz;
        double step_y = M_PI / res_y;

        for (int j = 1; j < res_y; j++) {
            for (int i = 0; i < res_xz; i++) {
                double x = cos(i * step_xz) * sin(j * step_y);
                double y = cos(j * step_y);
                double z = sin(i * step_xz) * sin(j * step_y);

                vertices.push_back(Vector(x, y, z));
                normals.push_back(Vector(x, y, z));
            }
        }

        //Add top and bottom vertices at 1 and -1
        vertices.push_back(Vector(0, 1, 0));
        normals.push_back(Vector(0, 1, 0));
        vertices.push_back(Vector(0, -1, 0));
        normals.push_back(Vector(0, -1, 0));

        int top = vertices.size() - 2;
        int bottom = vertices.size() - 1;

        //Add top and bottom triangles
        for (int i = 0; i < res_xz; i++) {
            va.push_back(top);
            va.push_back(i);
            va.push_back((i + 1) % res_xz);

            na.push_back(top);
            na.push_back(i);
            na.push_back((i + 1) % res_xz);
        }

        int bottom_start = vertices.size() - 2 - res_xz;

        for (int i = 0; i < res_xz; i++) {
            va.push_back(bottom);
            va.push_back(bottom_start + i);
            va.push_back(bottom_start + (i + 1) % res_xz);

            na.push_back(bottom);
            na.push_back(bottom_start + i);
            na.push_back(bottom_start + (i + 1) % res_xz);
        }

        //Add middle triangles
        for (int j = 0; j < res_y - 2; j++) {
            for (int i = 0; i < res_xz; i++) {
                va.push_back(j * res_xz + i);
                va.push_back(j * res_xz + (i + 1) % res_xz);
                va.push_back((j + 1) * res_xz + i);

                na.push_back(j * res_xz + i);
                na.push_back(j * res_xz + (i + 1) % res_xz);
                na.push_back((j + 1) * res_xz + i);

                va.push_back(j * res_xz + (i + 1) % res_xz);
                va.push_back((j + 1) * res_xz + (i + 1) % res_xz);
                va.push_back((j + 1) * res_xz + i);

                na.push_back(j * res_xz + (i + 1) % res_xz);
                na.push_back((j + 1) * res_xz + (i + 1) % res_xz);
                na.push_back((j + 1) * res_xz + i);
            }
        }

        

        return Mesh(vertices, normals, va, na);
    }

    Mesh initHalfSphere(int res_xz, int res_y) {
        std::vector<Vector> vertices;
        std::vector<Vector> normals;
        std::vector<int> va;
        std::vector<int> na;

        double step_xz = 2 * M_PI / res_xz;
        double step_y = M_PI / (res_y*2);

        for (int i = 1; i < res_y + 1; i++)
        {
            double alpha = i * step_y;
            for (int j = 0; j < res_xz; j++)
            {
                double beta = j * step_xz;
                double x = cos(beta) * sin(alpha);
                double y = cos(alpha);
                double z = sin(beta) * sin(alpha);

                vertices.push_back(Vector(x, y, z));
                normals.push_back(Vector(x, y, z));
            }
        }

        // Add top vertex at 1
        vertices.push_back(Vector(0, 1, 0));
        normals.push_back(Vector(0, 1, 0));

        // Remember the index of the top vertex
        int top = vertices.size() - 1;

        // Add top triangles
        for (int i = 0; i < res_xz; i++)
        {
            va.push_back(top);
            va.push_back(i);
            va.push_back((i + 1) % res_xz);

            na.push_back(top);
            na.push_back(i);
            na.push_back((i + 1) % res_xz);
        }

        for (int i = 0; i < res_y - 1; i++) {
            for (int j = 0; j < res_xz; j++)
            {
                va.push_back(i * res_xz + j); // i,j
                va.push_back((i + 1) * res_xz + j); // i+1,j
                va.push_back((i + 1) * res_xz + (j + 1) % res_xz); // i+1,j+1

                na.push_back(i * res_xz + j); // i,j
                na.push_back((i + 1) * res_xz + j); // i+1,j
                na.push_back((i + 1) * res_xz + (j + 1) % res_xz); // i+1,j+1

                va.push_back(i * res_xz + j); // i,j
                va.push_back((i + 1) * res_xz + (j + 1) % res_xz); // i+1,j+1
                va.push_back(i * res_xz + (j + 1) % res_xz); // i,j+1

                na.push_back(i * res_xz + j); // i,j
                na.push_back((i + 1) * res_xz + (j + 1) % res_xz); // i+1,j+1
                na.push_back(i * res_xz + (j + 1) % res_xz); // i,j+1
            }
        }

        return Mesh(vertices, normals, va, na);
        
    }

    Mesh initTore(double radius, double thickness, int res_hring, int res_vring) {
        std::vector<Vector> vertices;
        std::vector<Vector> normals;
        std::vector<int> va;
        std::vector<int> na;

        // Fill std::vectors with tore vertices and normals
        double step_hring = 2 * M_PI / res_hring;
        double step_vring = 2 * M_PI / res_vring;

        for (int j = 0; j < res_vring; j++) {
            double alpha = j * step_vring;
            double local_x = thickness * cos(alpha);
            double local_y = thickness * sin(alpha);
            for (int i = 0; i < res_hring; i++) {
                double theta = i * step_hring;
                double x = (radius + local_x) * cos(theta);
                double z = (radius + local_x) * sin(theta);
                double y = local_y;

                vertices.push_back(Vector(x, y, z));
                normals.push_back(Vector(
                    local_x * cos(theta),
                    y,
                    local_x * sin(theta)
                ));
            }
        }

        // Add tore triangles
        for (int j = 0; j < res_vring; j++) {
            for (int i = 0; i < res_hring; i++) {
                va.push_back(j * res_hring + i);
                va.push_back(j * res_hring + (i + 1) % res_hring);
                va.push_back(((j + 1) % res_vring) * res_hring + i);

                na.push_back(j * res_hring + i);
                na.push_back(j * res_hring + (i + 1) % res_hring);
                na.push_back(((j + 1) % res_vring) * res_hring + i);

                va.push_back(j * res_hring + (i + 1) % res_hring);
                va.push_back(((j + 1) % res_vring) * res_hring + (i + 1) % res_hring);
                va.push_back(((j + 1) % res_vring) * res_hring + i);

                na.push_back(j * res_hring + (i + 1) % res_hring);
                na.push_back(((j + 1) % res_vring) * res_hring + (i + 1) % res_hring);
                na.push_back(((j + 1) % res_vring) * res_hring + i);
            }
        }

        return Mesh(vertices, normals, va, na);
    }

    Mesh initCyllinder(double height, int res_disque) {
        std::vector<Vector> vertices;
        std::vector<Vector> normals;
        std::vector<int> va;
        std::vector<int> na;

        // Fill std::vectors with cyllindre vertices and normals
        double step_disque = 2 * M_PI / res_disque;

        for (int i = 0; i < res_disque; i++) {
            double x = cos(i * step_disque);
            double y = height / 2;
            double z = sin(i * step_disque);

            vertices.push_back(Vector(x, y, z));
            normals.push_back(Normalized(Vector(x, 0, z)));
        }

        for (int i = 0; i < res_disque; i++) {
            double x = cos(i * step_disque);
            double y = -height / 2;
            double z = sin(i * step_disque);

            vertices.push_back(Vector(x, y, z));
            normals.push_back(Normalized(Vector(x, 0, z)));
        }

        // Add cyllindre triangles
        for (int i = 0; i < res_disque; i++) {
            va.push_back(i);
            va.push_back((i + 1) % res_disque);
            va.push_back(res_disque + i);

            na.push_back(i);
            na.push_back((i + 1) % res_disque);
            na.push_back(res_disque + i);

            va.push_back((i + 1) % res_disque);
            va.push_back(res_disque + (i + 1) % res_disque);
            va.push_back(res_disque + i);

            na.push_back((i + 1) % res_disque);
            na.push_back(res_disque + (i + 1) % res_disque);
            na.push_back(res_disque + i);
        }

        return Mesh(vertices, normals, va, na);
    }

    Mesh initCapsule(double height, int res_disque, int res_dome) {
        // Capsule =
                 // 1 cyllindre
        return initCyllinder(height, res_disque)
                 // 1 demi-sphere
               + initHalfSphere(res_disque, res_dome)
                    .Translate(Vector(0, height / 2, 0))
                 // 1 demi-sphere (inverted)
               + (
                    initHalfSphere(res_disque, res_dome)
                        * RotateX(M_PI)
               ).Translate(Vector(0, -height / 2, 0))
        ;
    }

}